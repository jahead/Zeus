namespace Zeus
module Base =
    open Fable.Core

    [<Erase>]
    type Value =
    | Number of int
    | String of string

    // [<StringEnum>]
    // type Operator =
    // | GreaterThan
    // | LessThan
    // | EqualTo
    // | Contains
    // | ProvidedAnAnswer
    // | Unknown

    [<StringEnum>]
    type Operator =
    | [<CompiledName("GreaterThan")>] GreaterThan
    | [<CompiledName("LessThan")>] LessThan
    | [<CompiledName("EqualTo")>] EqualTo
    | [<CompiledName("Contains")>] Contains
    | [<CompiledName("ProvidedAnAnswer")>] ProvidedAnAnswer
    | Unknown

    let createOperator(op: Operator) =
        match op with
        | GreaterThan ->  fun y x -> x >= y
        | LessThan ->     fun y x -> x < y
        | EqualTo ->      fun y x -> x = y
        | ProvidedAnAnswer -> fun y x ->
            match (y) with
            | String(y) -> y.Length > 0
            | _ -> failwith "ProvidedAnAnswer only handles strings"
        | Contains ->     fun y x ->
            match (y, x) with
            | (String(y), String(x)) -> x.Contains(y)
            | _ -> failwith "x and y must be a String"
        | _ -> failwithf "Unknown operator %O" op

    type Answer = { value:Value; questionId:int; pageId:int }

module Input =
    open Base

    type InputRuleType = { code:int; value:Operator}

    type InputValueType = { code:int; value:Value}

    type InputRule = { answerValue:InputValueType; questionId:int; ruleType:InputRuleType; pageId:int}

    type InputGroup = { id:int; rules:InputRule[] }

    type InputPage = { id:int; constraints:InputGroup[] }

    type InputQuestionnaire = { id:int; pages:InputPage [] }

module Dictator =
    open Base
    open Input

    type Rule(op: (Value -> bool), questionId, pageId) =
        member this.Operator = op
        member this.QuestionId = questionId
        member this.PageId = pageId

    type Group (rules : InputRule[]) =
        let createRule (inputRule:InputRule):Rule =
            let op = createOperator(inputRule.ruleType.value) inputRule.answerValue.value
            Rule(op, inputRule.questionId, inputRule.pageId)

        let createRules (inputRules:InputRule[]):Rule[] =
            Array.map(createRule) inputRules

        member this.IsTrue(answers:Answer[]) =
            Array.forall(fun (x:Rule) ->
                let retV = Array.tryFind(fun (i:Answer) -> i.questionId = x.QuestionId && i.pageId = x.PageId) answers
                if retV = None then
                    false
                else
                    x.Operator(retV.Value.value)) this.Rules

        member this.Rules:Rule[] = createRules rules

    type Page (id:int, groups:Input.InputGroup[]) =
        let createGroupsArray (groups:InputGroup[]) =
            let arr = Array.sortBy(fun (x:InputGroup) -> x.id) groups
            Array.map(fun x -> Group(x.rules)) arr

        member this.PageId = id

        member this.Groups = createGroupsArray groups

    type Manifest (questionnairePageRules:InputQuestionnaire) =
        let createPagesArray (pageRules:InputPage []):Page[] =
            let arr = Array.sortBy(fun (x:InputPage) -> x.id) pageRules
            Array.map(fun (x:InputPage) -> Page(x.id, x.constraints)) arr

        member this.CanView(pageId, answers:Answer[]):bool =
            let page:Page = Array.find(fun x -> x.PageId = pageId) this.Pages
            if page.Groups.Length = 0 then
                true
            else
                Array.fold(fun acc (item:Group) -> item.IsTrue(answers) || acc) false page.Groups

        member this.Pages:Page[] = createPagesArray questionnairePageRules.pages

    let createManifest (questionnairePageRules) =
            Manifest(questionnairePageRules)

    let printOperator (input:Operator) =
        match input with
            | GreaterThan -> "GreaterThan"
            | LessThan ->  "LessThan"
            | EqualTo ->  "EqualTo"
            | Contains ->  "Contains"
            | _ -> failwith "Unknown operator"
